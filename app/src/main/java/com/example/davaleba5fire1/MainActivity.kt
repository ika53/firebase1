package com.example.davaleba5fire1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmailAddress: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordrepeat: EditText
    private lateinit var buttonregister: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registerListener()
    }
    private fun init() {
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPasswordrepeat = findViewById(R.id.editTextPasswordrepeat)
        buttonregister = findViewById(R.id.buttonregister)
    }
    private fun registerListener() {

        buttonregister.setOnClickListener {

            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()
            val repeatpassword = editTextPasswordrepeat.text.toString()

            if (email.isEmpty() && password.isEmpty() && repeatpassword.isEmpty()){
                editTextEmailAddress.error = "შეიყვანეთ ინფორმაცია"
                editTextPassword.error = "შეიყვანეთ ინფორმაცია"
                editTextPasswordrepeat.error = "შეიყვანეთ ინფორმაცია"
                return@setOnClickListener
            }
            if (!(email.contains("@"))){
                editTextEmailAddress.error = "შეიყვანეთ მეილი სწორად"
                return@setOnClickListener

            }
            if ( password.length <= 8 && !(password.matches(".*[0-9].*".toRegex())) && password != repeatpassword){
                editTextPassword.error = "შეიყვანეთ სწორად"
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                            Toast.makeText(this,"ძლივსს", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "მუდგარენ ხვადუ", Toast.LENGTH_SHORT).show()
                    }
                }




        }



    }
}